//
//  TimerViewDataSource.m
//  UISDK
//
//  Created by PC Nguyen on 4/25/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "TimerViewDataSource.h"

@implementation TimerViewDataSource

- (id)init
{
	if (self = [super init]) {
		self.counter = 7;
		
		[NSTimer scheduledTimerWithTimeInterval:1.0
										 target:self
									   selector:@selector(continuousCount)
									   userInfo:nil
										repeats:YES];
	}

	return self;
}

- (void)continuousCount
{
	self.counter += 1;
}

- (NSString *)counterText
{
	return [NSString stringWithFormat:@"%d", (int)self.counter];
}

#pragma mark - Override for Selective Update

- (NSDictionary *)propertyUpdateInfo
{
	return @{@"counterText": @"counter"};
}

@end
