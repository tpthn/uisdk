//
//  TimerView.h
//  UISDK
//
//  Created by PC Nguyen on 5/9/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerView : UIView

@property (nonatomic, strong) UILabel *timerLabel;

@end
