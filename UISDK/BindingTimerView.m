//
//  BindingTimerView.m
//  UISDK
//
//  Created by PC Nguyen on 5/8/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "BindingTimerView.h"

@interface BindingTimerView ()

@property (nonatomic, strong) UILabel *timerLabel;

@end

@implementation BindingTimerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		[self addSubview:self.timerLabel];
    }
    return self;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	self.timerLabel.frame = [self timerFrame];
}

- (Class)ui_binderClass
{
	return [TimerViewDataSource class];
}

- (NSDictionary *)ui_bindingInfo
{
	return @{ @"timerLabel.text": @"counterText" };
}

#pragma mark - Timer Label

- (CGRect)timerFrame
{
	return self.bounds;
}

- (UILabel *)timerLabel
{
	if (!_timerLabel) {
		_timerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
		_timerLabel.textColor = [UIColor redColor];
		_timerLabel.font = [UIFont boldSystemFontOfSize:54.0f];
		_timerLabel.textAlignment = NSTextAlignmentCenter;
	}
	
	return _timerLabel;
}


@end
