//
//  AppDelegate.m
//  UISDK
//
//  Created by PC Nguyen on 3/19/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "AppDelegate.h"

#import "TimerViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
	
	TimerViewController *timerViewController = [[TimerViewController alloc] init];
	self.window.rootViewController = timerViewController;
	
    [self.window makeKeyAndVisible];
    return YES;
}

@end
