//
//  TimerView.m
//  UISDK
//
//  Created by PC Nguyen on 5/9/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "TimerView.h"

@implementation TimerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		[self addSubview:self.timerLabel];
    }
    return self;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	self.timerLabel.frame = [self timerFrame];
}

#pragma mark - Timer Label

- (CGRect)timerFrame
{
	return self.bounds;
}

- (UILabel *)timerLabel
{
	if (!_timerLabel) {
		_timerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
		_timerLabel.textColor = [UIColor redColor];
		_timerLabel.font = [UIFont boldSystemFontOfSize:54.0f];
		_timerLabel.textAlignment = NSTextAlignmentCenter;
	}
	
	return _timerLabel;
}

@end
