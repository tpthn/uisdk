//
//  BindingTimerView.h
//  UISDK
//
//  Created by PC Nguyen on 5/8/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIView+DataBinding.h"
#import "TimerViewDataSource.h"

@interface BindingTimerView : UIView <USViewDataBinding>

@end
