//
//  TimerViewController.m
//  UISDK
//
//  Created by PC Nguyen on 4/25/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "TimerViewController.h"

#import "TimerView.h"
#import "BindingTimerView.h"

#import "TimerViewDataSource.h"
#import "UIViewController+DataBinding.h"

@interface TimerViewController () <USViewDataBinding>

@property (nonatomic, strong) BindingTimerView *bindingTimerView;
@property (nonatomic, strong) TimerView *timerView;

@end

@implementation TimerViewController

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	[self.view addSubview:self.timerView];
	[self.view addSubview:self.bindingTimerView];
	
	[self ui_handleBindinUpdate];
}

#pragma mark - Binding Timer View - Which Update Itself According to its binding

- (CGRect)bindingTimerFrame
{
	CGRect frame = self.view.bounds;
	frame.size.height /= 2;
	return frame;
}

- (BindingTimerView *)bindingTimerView
{
	if (!_bindingTimerView) {
		_bindingTimerView = [[BindingTimerView alloc] initWithFrame:[self bindingTimerFrame]];
		_bindingTimerView.backgroundColor = [UIColor blueColor];
	}
	
	return _bindingTimerView;
}

#pragma mark - Timer View - Which Update By Binding on this controller

- (CGRect)timerFrame
{
	CGRect frame = self.view.bounds;
	frame.size.height /= 2;
	frame.origin.y = frame.size.height;
	
	return frame;
}

- (TimerView *)timerView
{
	if (!_timerView) {
		_timerView = [[TimerView alloc] initWithFrame:[self timerFrame]];
		_timerView.backgroundColor = [UIColor greenColor];
	}
	
	return _timerView;
}

#pragma mark - USViewDataBinding Protocol

- (Class)ui_binderClass
{
	return [TimerViewDataSource class];
}

- (NSDictionary *)ui_bindingInfo
{
	return @{@"timerView.timerLabel.text" : @"counterText"};
}

@end
