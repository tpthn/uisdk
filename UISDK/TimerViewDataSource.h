//
//  TimerViewDataSource.h
//  UISDK
//
//  Created by PC Nguyen on 4/25/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "USViewDataSource.h"

@interface TimerViewDataSource : USViewDataSource

@property (nonatomic, assign) NSInteger counter;

- (NSString *)counterText;

@end
