Pod::Spec.new do |spec|
  spec.name         = 'UISDK'
  spec.version      = '0.1.0'
  spec.license      = 'MIT' 
  spec.homepage     = 'https://bitbucket.org/tpthn/uisdk'
  spec.authors      = { 'PC Nguyen' => 'tpthn@yahoo.com' }
  spec.summary      = 'Collection Of Useful UI Stuff'
  spec.source       = { :git => 'https://tpthn@bitbucket.org/tpthn/uisdk.git',
						:branch => 'master' }

  spec.requires_arc = true
  spec.ios.deployment_target = '6.0'
  spec.source_files = 'UISDKLib/*.{h,m}'
  spec.frameworks   = 'UIKit', 'CoreGraphics', 'QuartzCore'
end