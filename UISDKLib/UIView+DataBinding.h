//
//  UIView+DataBinding.h
//  UISDK
//
//  Created by PC Nguyen on 5/8/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "USViewDataSource.h"

@interface UIView (DataBinding) <USViewDataSourceBindingDelegate>

- (USViewDataSource *)ui_currentBinderSource;

@end
