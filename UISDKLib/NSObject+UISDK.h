//
//  NSObject+UISDK.h
//  UISDK
//
//  Created by PC Nguyen on 4/25/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (UISDK)

#pragma mark - Swizzling

+ (void)ns_swizzleSelector:(SEL)originalSelector bySelector:(SEL)swizzledSelector;

#pragma mark - Associate Object

- (id)ns_setAssociateObjectWithSelector:(SEL)objectSelector;

- (id)ns_associateObjectForSelector:(SEL)selector;

#pragma mark - Perform Selector

- (id)ns_objectFromSelector:(SEL)selector;

- (void)ns_performSelector:(SEL)selector withObject:(id)object;

@end
