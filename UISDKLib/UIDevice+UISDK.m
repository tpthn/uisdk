//
//  UIDevice+UISDK.m
//  UISDK
//
//  Created by PC Nguyen on 3/19/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "UIDevice+UISDK.h"

@implementation UIDevice (UISDK)

#pragma mark - OS Version

+ (NSInteger)ui_versionMajor
{
    NSInteger majorVersion = [self versionComponentAtIndex:0];
    
    return majorVersion;
}

+ (NSInteger)ui_versionMinor
{
    NSInteger minorVersion = [self versionComponentAtIndex:1];
    
    return minorVersion;
}

+ (BOOL)ui_compatibleWithMajorVersion:(NSInteger)version
{
    BOOL compatible = ([self ui_versionMajor] >= version);
    
    return compatible;
}

+ (BOOL)ui_matchMajorVersion:(NSInteger)version
{
    BOOL compatible = ([self ui_versionMajor] == version);
    
    return compatible;
}

#pragma mark - OS Version Private

+ (NSInteger)versionComponentAtIndex:(NSInteger)componentIndex
{
    NSInteger componentValue = 0;
    NSArray *versionComponents = [[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."];
    
    if ([versionComponents count] > componentIndex) {
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        NSString *componentString = [versionComponents objectAtIndex:componentIndex];
        componentValue = [[numberFormatter numberFromString:componentString] integerValue];
    } else {
        
    }
    
    return componentValue;
}

#pragma mark - Screen Size

+ (BOOL)ui_isWideScreen {
	return [[UIScreen mainScreen] bounds].size.height == 568.0f;
}

@end
