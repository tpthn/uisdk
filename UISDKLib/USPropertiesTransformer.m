//
//  USPropertiesTransformer.m
//  UISDK
//
//  Created by PC Nguyen on 5/8/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "USPropertiesTransformer.h"

NSString *const USPropertiesTransformerKey = @"USPropertiesTransformerKey";

@implementation USPropertiesTransformer

+ (void)load
{
	USPropertiesTransformer *propertiesTransformer = [[USPropertiesTransformer alloc] init];
	
	[NSValueTransformer setValueTransformer:propertiesTransformer forName:USPropertiesTransformerKey];
}

+ (Class)transformedValueClass
{
	return [NSArray class];
}

+ (BOOL)allowsReverseTransformation
{
	return NO;
}

- (id)transformedValue:(id)value
{
	NSMutableArray *selectors = nil;
	
	if ([value isKindOfClass:[NSString class]]) {
		NSArray *component = [value componentsSeparatedByString:@"."];
		if ([component count] > 1) {
			selectors = [NSMutableArray arrayWithArray:component];
			[selectors removeLastObject];
		}
	}
	
	return selectors;
}

+ (NSValueTransformer *)transformer
{
	return [NSValueTransformer valueTransformerForName:USPropertiesTransformerKey];
}

@end
