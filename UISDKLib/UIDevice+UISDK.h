//
//  UIDevice+UISDK.h
//  UISDK
//
//  Created by PC Nguyen on 3/19/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (UISDK)

#pragma mark - OS Version Compatibility

+ (NSInteger)ui_versionMajor;
+ (NSInteger)ui_versionMinor;

+ (BOOL)ui_compatibleWithMajorVersion:(NSInteger)version;
+ (BOOL)ui_matchMajorVersion:(NSInteger)version;

#pragma mark - Screen Size

+ (BOOL)ui_isWideScreen;

@end
