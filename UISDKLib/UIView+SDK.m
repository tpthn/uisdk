//
//  UIView+SDK.m
//  UISDK
//
//  Created by PC Nguyen on 5/14/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "UIView+SDK.h"

@implementation UIView (SDK)

- (void)ui_rightFlipToView:(UIView *)toView duration:(CGFloat)duration
{
	[self ui_rightFlipToView:toView duration:duration completion:^{}];
}

- (void)ui_rightFlipToView:(UIView *)toView duration:(CGFloat)duration completion:(void (^)())completion
{
	[self __flip:UIViewAnimationOptionTransitionFlipFromRight toView:toView duration:duration completion:completion];
}

- (void)ui_leftFlipToView:(UIView *)toView duration:(CGFloat)duration
{
	[self ui_leftFlipToView:toView duration:duration completion:^{}];
}

- (void)ui_leftFlipToView:(UIView *)toView duration:(CGFloat)duration completion:(void (^)())completion
{
	[self __flip:UIViewAnimationOptionTransitionFlipFromRight toView:toView duration:duration completion:completion];
}

#pragma mark - Private

- (void)__flip:(UIViewAnimationOptions)transition toView:(UIView *)toView duration:(CGFloat)duration completion:(void (^)())completion
{
	toView.hidden = NO;
	
	[UIView transitionFromView:self
						toView:toView
					  duration:duration
					   options:UIViewAnimationOptionTransitionFlipFromRight
					completion:^(BOOL finished) {
						self.hidden = YES;
						completion();
					}];
}

@end
