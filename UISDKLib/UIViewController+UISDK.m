//
//  UIViewController+UISDK.m
//  UISDK
//
//  Created by PC Nguyen on 3/19/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "UIViewController+UISDK.h"
#import "UIDevice+UISDK.h"

@implementation UIViewController (UISDK)

- (void)ui_adjustIOS7Boundaries
{
	if ([UIDevice ui_compatibleWithMajorVersion:7]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = YES;
    }
}

@end
