//
//  USSubscribedDataSource.h
//  UISDK
//
//  Created by PC Nguyen on 5/7/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "USViewDataSource.h"

@interface USManagedDataSource : USViewDataSource

@property (nonatomic, strong) NSString *managedService;

#pragma mark - Managed Service

/***
 * register this dataSource with USDataSourceManager
 */
- (void)setManagedService:(NSString *)managedService;

/***
 * unRegister this dataSource from USDataSourceManager
 * 
 * IMPORTANT:Should be called on when dealloc view
 */
- (void)removeCurrentManagedService;

#pragma mark - Subclass Hook

/***
 * this get called on [USDataSourceManager notifyDataSourcesOfService:]
 * if managedService match notified service
 */
- (void)handleManagedDataChanged;

@end
