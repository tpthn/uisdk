//
//  NSObject+UISDK.m
//  UISDK
//
//  Created by PC Nguyen on 4/25/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "NSObject+UISDK.h"

#import <objc/runtime.h>

@implementation NSObject (UISDK)

#pragma mark - Swizzling

+ (void)ns_swizzleSelector:(SEL)originalSelector bySelector:(SEL)swizzledSelector
{
	static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
		
        Method originalMethod = class_getInstanceMethod(class, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
		
        BOOL didAddMethod =
		class_addMethod(class,
						originalSelector,
						method_getImplementation(swizzledMethod),
						method_getTypeEncoding(swizzledMethod));
		
        if (didAddMethod) {
            class_replaceMethod(class,
								swizzledSelector,
								method_getImplementation(originalMethod),
								method_getTypeEncoding(originalMethod));
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
    });
}

#pragma mark - Associate Object

- (id)ns_setAssociateObjectWithSelector:(SEL)objectSelector
{
	id value = [self ns_objectFromSelector:objectSelector];
	
	if (value) {
		objc_setAssociatedObject(self, objectSelector, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	} else {
		NSLog(@"Exception: Setting Empty Associate Object");
	}
	
	return value;
}

- (id)ns_associateObjectForSelector:(SEL)selector
{
	id object = objc_getAssociatedObject(self, selector);
	return object;
}

#pragma mark - Perform Selector

- (id)ns_objectFromSelector:(SEL)selector
{
	if ([self respondsToSelector:selector]) {
		IMP implementation = [self methodForSelector:selector];
		id (*function)(id, SEL) = (void *)implementation;
		return function(self, selector);
	}
	
	NSLog(@"%@ not respond to selector %@", NSStringFromClass([self class]), NSStringFromSelector(selector));
	return nil;
}

- (void)ns_performSelector:(SEL)selector withObject:(id)object
{
	if ([self respondsToSelector:selector]) {
		IMP implementation = [self methodForSelector:selector];
		void (*function)(id, SEL, id) = (void *)implementation;
		function(self, selector, object);
	} else {
		NSLog(@"%@ not respond to selector %@", NSStringFromClass([self class]), NSStringFromSelector(selector));
	}
}

@end
