//
//  UIView+DataBinding.m
//  UISDK
//
//  Created by PC Nguyen on 5/8/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "UIView+DataBinding.h"

#import "USPropertiesTransformer.h"
#import "USSetterTransformer.h"

#import "NSObject+UISDK.h"

@implementation UIView (DataBinding) 

+ (void)load
{
	[self ns_swizzleSelector:@selector(layoutSubviews) bySelector:@selector(ui_swizzlingLayoutSubViews)];
}

#pragma mark - SWIZZLING - DANGEROUS STUFF

- (void)ui_swizzlingLayoutSubViews
{
	if ([[self ui_currentBinderSource] shouldReloadWithLayoutUpdate]) {
		[self ui_handleBindinUpdate];
	}
	
	[self ui_swizzlingLayoutSubViews];
}

#pragma mark - Auto Binding

- (void)ui_handleBindingUpdateValue:(id)key withBindedValue:(id)bindingKey
{
	//--getter
	NSArray *getterProperties = [[USPropertiesTransformer transformer] transformedValue:bindingKey];
	id getterOwner = [self ui_binderSource];
	
	for (NSString *childProperties in getterProperties) {
		getterOwner = [self ui_objectFromString:childProperties owner:getterOwner];
	}
	
	NSString *getterSelectorString = [[USSetterTransformer transformer] reverseTransformedValue:bindingKey];
	id getterValue = [self ui_objectFromString:getterSelectorString owner:getterOwner];
	
	//--setter
	NSArray *setterProperties = [[USPropertiesTransformer transformer] transformedValue:key];
	id setterOwner = self;
	
	for (NSString *childProperties in setterProperties) {
		setterOwner = [self ui_objectFromString:childProperties owner:setterOwner];
	}
	
	NSString *setterSelectorString = [[USSetterTransformer transformer] transformedValue:key];
	SEL setterSelector = NSSelectorFromString(setterSelectorString);
	
	//--bind value
	[setterOwner ns_performSelector:setterSelector withObject:getterValue];
}

- (void)ui_handleBindinUpdate
{
	NSDictionary *binding = [self ui_bindingInfo];
	
	[binding enumerateKeysAndObjectsUsingBlock:^(id uiValue, id sourceValue, BOOL *stop) {
		[self ui_handleBindingUpdateValue:uiValue withBindedValue:sourceValue];
	}];
}

- (USViewDataSource *)ui_binderSource
{
	USViewDataSource *dataBinder = [self ns_associateObjectForSelector:@selector(ui_binderSourceAssociate)];
	
	if (!dataBinder) {
		dataBinder = [self ns_setAssociateObjectWithSelector:@selector(ui_binderSourceAssociate)];
	}
	
	return dataBinder;
}

- (USViewDataSource *)ui_binderSourceAssociate
{
	USViewDataSource *dataBinder = nil;
	Class binderClass = [self ui_binderClass];
	
	if ([binderClass isSubclassOfClass:[USViewDataSource class]]) {
		dataBinder = [[binderClass alloc] init];
		dataBinder.bindingDelegate = self;
	}
	
	return dataBinder;
}

#pragma mark - USViewAutoBinding Protocol Mock

- (Class)ui_binderClass
{
	return [USViewDataSource class];
}

- (NSDictionary *)ui_bindingInfo
{
	return [NSDictionary dictionary];
}

#pragma mark - Public Method

- (USViewDataSource *)ui_currentBinderSource
{
	USViewDataSource *dataBinder = nil;
	
	if ([self isBindingMode]) {
		dataBinder = [self ui_binderSource];
	}
	
	return dataBinder;
}

#pragma mark - RPViewDataSourceDelegate

- (void)viewDataSource:(USViewDataSource *)dataSource updateBindingKey:(NSString *)bindKey
{
	if (dataSource.shouldUpdateLayout) {
		[self layoutSubviews];
	} else {
		if ([self isBindingMode]) {
			[[self ui_bindingInfo] enumerateKeysAndObjectsUsingBlock:^(id uiValue, id sourceValue, BOOL *stop){
				if ([sourceValue isEqualToString:bindKey]) {
					[self ui_handleBindingUpdateValue:uiValue withBindedValue:sourceValue];
				}
			}];
		}
	}
}

- (void)viewDataSourceUpdateAllBindingKey:(USViewDataSource *)dataSource
{
	if (dataSource.shouldUpdateLayout) {
		[self layoutSubviews];
	} else {
		if ([self isBindingMode]) {
			[self ui_handleBindinUpdate];
		}
	}
}

#pragma mark - Private

- (BOOL)isBindingMode
{
	BOOL isBinding =  [[self class] conformsToProtocol:@protocol(USViewDataBinding)];
	return isBinding;
}

- (id)ui_objectFromString:(NSString *)selectorString owner:(id)owner;
{
	id returnedObject = [owner ns_objectFromSelector:NSSelectorFromString(selectorString)];
	
	return returnedObject;
}

@end
