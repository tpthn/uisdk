//
//  UIViewController+DataBinding.h
//  UISDK
//
//  Created by PC Nguyen on 5/9/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "USViewDataSource.h"

@interface UIViewController (DataBinding) <USViewDataSourceBindingDelegate>

- (USViewDataSource *)ui_currentBinderSource;

- (void)ui_handleBindinUpdate;

@end
