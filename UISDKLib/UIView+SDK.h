//
//  UIView+SDK.h
//  UISDK
//
//  Created by PC Nguyen on 5/14/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SDK)

#pragma mark - Animation

- (void)ui_rightFlipToView:(UIView *)toView duration:(CGFloat)duration;
- (void)ui_rightFlipToView:(UIView *)toView duration:(CGFloat)duration completion:(void (^)())completion;

- (void)ui_leftFlipToView:(UIView *)toView duration:(CGFloat)duration;
- (void)ui_leftFlipToView:(UIView *)toView duration:(CGFloat)duration completion:(void (^)())completion;

@end
