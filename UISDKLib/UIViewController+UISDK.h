//
//  UIViewController+UISDK.h
//  UISDK
//
//  Created by PC Nguyen on 3/19/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (UISDK)

- (void)ui_adjustIOS7Boundaries;

@end
