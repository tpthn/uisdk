//
//  USDataSourceManager.h
//  UISDK
//
//  Created by PC Nguyen on 5/7/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class USManagedDataSource;

@interface USDataSourceManager : NSObject

+ (instancetype)sharedManager;

- (void)registerDataSource:(USManagedDataSource *)dataSource forService:(NSString *)serviceName;

- (void)unRegisterDataSource:(USManagedDataSource *)dataSource fromService:(NSString *)serviceName;

- (void)notifyDataSourcesOfService:(NSString *)serviceName;

@end
