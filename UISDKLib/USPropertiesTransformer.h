//
//  USPropertiesTransformer.h
//  UISDK
//
//  Created by PC Nguyen on 5/8/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const USPropertiesTransformerKey;

@interface USPropertiesTransformer : NSValueTransformer

+ (NSValueTransformer *)transformer;

@end
