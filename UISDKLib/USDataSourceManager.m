//
//  USDataSourceManager.m
//  UISDK
//
//  Created by PC Nguyen on 5/7/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "USDataSourceManager.h"
#import "USManagedDataSource.h"

@interface USDataSourceManager ()

@property (nonatomic, strong) NSMutableDictionary *registeredDataSources;

@end

@implementation USDataSourceManager

+ (instancetype)sharedManager
{
	static USDataSourceManager *manager;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		manager = [[USDataSourceManager alloc] init];
		
	});
	
	return manager;
}

- (NSMutableDictionary *)registeredDataSources
{
	if (!_registeredDataSources) {
		_registeredDataSources = [[NSMutableDictionary alloc] init];
	}
	
	return _registeredDataSources;
}

- (void)notifyDataSourcesOfService:(NSString *)serviceName
{
	NSSet *dataSources = [self dataSourcesForService:serviceName];
	
	for (USManagedDataSource *subscribedDataSource in dataSources) {
		[subscribedDataSource handleManagedDataChanged];
	}
	
}

- (void)registerDataSource:(USManagedDataSource *)dataSource forService:(NSString *)serviceName
{
	NSSet *existingDataSources = [self dataSourcesForService:serviceName];
	NSMutableSet *modifiedDataSources = [existingDataSources mutableCopy];
	
	if (dataSource) {
		[modifiedDataSources addObject:dataSource];
		[self.registeredDataSources setValue:modifiedDataSources forKey:serviceName];
	}
}

- (void)unRegisterDataSource:(USManagedDataSource *)dataSource fromService:(NSString *)serviceName
{
	NSSet *existingDataSources = [self dataSourcesForService:serviceName];
	NSMutableSet *modifiedDataSources = [existingDataSources mutableCopy];
	
	if ([modifiedDataSources containsObject:dataSource]) {
		[modifiedDataSources removeObject:dataSource];
		[self.registeredDataSources setValue:modifiedDataSources forKey:serviceName];
	}
}

#pragma mark - Private

- (NSSet *)dataSourcesForService:(NSString *)serviceName
{
	NSSet *serviceDataSources = [self.registeredDataSources valueForKey:serviceName];
	if (serviceDataSources == nil) {
		serviceDataSources = [NSSet set];
	}
	
	return serviceDataSources;
}

@end
