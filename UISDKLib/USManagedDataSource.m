//
//  USSubscribedDataSource.m
//  UISDK
//
//  Created by PC Nguyen on 5/7/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "USManagedDataSource.h"
#import "USDataSourceManager.h"

@implementation USManagedDataSource

#pragma mark - Managed Service

- (void)setManagedService:(NSString *)managedService
{
	if ([self.managedService length] > 0) {
		[self removeCurrentManagedService];
	}
	
	_managedService = managedService;
	
	if ([managedService length] > 0) {
		[[USDataSourceManager sharedManager] registerDataSource:self forService:managedService];
	}
}

- (void)removeCurrentManagedService
{
	[[USDataSourceManager sharedManager] unRegisterDataSource:self fromService:self.managedService];
}

#pragma mark - Subclass Hook

- (void)handleManagedDataChanged
{
	/* Subclass Implementation */
}

@end
